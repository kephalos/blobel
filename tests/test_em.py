import pytest
from functools import reduce

import numpy as np
import scipy.stats as st
from matplotlib import pyplot
from collections import namedtuple
import operator


def _two_peak_data(*, N, p1, p2):
    delta = st.bernoulli(p1.pi).rvs(N)
    model1 = st.norm(loc=p1.m, scale=np.sqrt(p1.v)).rvs(N)
    model2 = st.norm(loc=p2.m, scale=np.sqrt(p2.v)).rvs(N)
    return np.where(delta, model1, model2)


def _likelihood(y, params):
    dists = [p.pi * st.norm(loc=p.m, scale=np.sqrt(p.v)).pdf(y) for p in params]
    return np.sum(np.log(sum(dists)))


Parameter = namedtuple('Parameter', ['m', 'v', 'pi'])


def _map(f, xs):
    return list(map(f, xs))


def weighted_mean(w, x):
    return np.inner(w, x) / np.sum(w)


def expectation_maximization_mixture(y, params):
    for i in range(10):
        pdfs = [p.pi * st.norm(loc=p.m, scale=np.sqrt(p.v)).pdf(y) for p in params]

        norm = reduce(operator.add, pdfs)
        expectations = _map(lambda x: x / norm, pdfs)
        means = [weighted_mean(e, y) for e in expectations]
        variances = [weighted_mean(e, (y - m)**2) for (m, e) in zip(means, expectations)]
        pis = _map(np.mean, expectations)
        params = [
            Parameter(pi=pi, v=v, m=m) for (pi, m, v, e) in zip(pis, means, variances, expectations)
        ]

    return params


def test_em_mixture_of_two():
    np.random.seed(0)
    sp = [Parameter(pi=0.6, m=3, v=0.8**2), Parameter(m=1, v=0.7**2, pi=0.4)]
    data = _two_peak_data(N=200, p1=sp[0], p2=sp[1])

    var = 1. / len(data) * np.sum((data - data.mean())**2)

    m1 = np.random.choice(data)
    m2 = np.random.choice(data)
    initial = [Parameter(m=m1, v=var, pi=0.5), Parameter(m=m2, v=var, pi=0.5)]

    converged_parameters = expectation_maximization_mixture(data, initial)

    np.testing.assert_allclose(converged_parameters[0].m, 1., atol=0.1)
    np.testing.assert_allclose(converged_parameters[1].m, 3., atol=0.1)


@pytest.mark.parametrize("i", list(range(10)))
def test_em_hastie_example(i):
    np.random.seed(i)
    data = [-0.39, 0.12, 0.94, 1.67, 1.76,
            2.44, 3.72, 4.28, 4.92, 5.53,
            0.06, 0.48, 1.01, 1.68, 1.80,
            3.25, 4.12, 4.6, 5.28, 6.22]
    data = np.asarray(data)

    var = 1. / len(data) * np.sum((data - data.mean())**2)

    m1 = np.random.choice(data)
    m2 = np.random.choice(data)

    initial = [Parameter(m=m1, v=var, pi=0.5), Parameter(m=m2, v=var, pi=0.5)]

    converged_parameters = expectation_maximization_mixture(data, initial)
    llr = _likelihood(data, initial) - _likelihood(data, converged_parameters)
    assert llr < 0.0
