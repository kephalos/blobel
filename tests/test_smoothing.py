import numpy as np


def test_kalman():

    t = np.arange(10)

    h0 = 10
    g = 9.81
    v = 1

    x_signal = v * t
    y_signal = h0 - 0.5 * g * t**2

    x_noise = np.random.randn(len(t))
    y_noise = np.random.randn(len(t))

    x = x_signal + x_noise
    y = y_signal + y_noise

    state = np.c_[x, y]
