======================================
Fitting To Orthogonal Polynomial Bases
======================================

Polynomial fits are notoriously tricky and often frowned upon. This gem from
(from the German introductory course book by :cite:`2012:blobel`  Blobel,
"Statistische und numerische Methoden der Datenanalyse", 2012), is a neat way
for univariate fits of data with orthogonal (more specifically orthonormal
polynomials).

Interface
=========


A fit for an orthogonal polynomial regression can be performed by

.. autofunction:: kvit.ortho.orthogonal_fit

The fit function returns a :class:`kvit.ortho.OrthogonalModel`. Which can be
used to estimate :math:`\hat{y}`, given a predictor `x`.


.. autoclass:: kvit.ortho.OrthogonalModel
   :members:


The orthonormal basis of the orthogonal polynomial regression can be
constructed using the auxilliary function

.. autofunction:: kvit.ortho.orthogonal_polynomials


Example
=======

.. plot::

   import numpy as np
   import pandas as pd
   from kvit.ortho import orthogonal_polynomials
   from kvit.ortho import orthogonal_fit

   x = 15 * np.random.randn(1000)
   y = 1.0 / (1 + np.exp(-x)) + 0.2 * np.random.randn(len(x))
   plt.figure()
   plt.plot(x, y, ",")
   plt.savefig("raw.png")
   plt.close("all")

   df = pd.DataFrame({"x": x, "y": y})
   df["n"] = pd.qcut(x, q=20).codes

   mv = df.groupby(["n"])["y"].agg(["mean", "var"])
   mv["n"] = mv.index

   x = mv["n"].values
   y = mv["mean"].values
   variance = mv["var"].values

   w = 1. / variance

   x_test = np.linspace(x.min(), x.max())

   plt.errorbar(x, y, yerr=variance, fmt="-o", label="data")
   for order in range(2, 8):
       model = orthogonal_fit(x, y, w, order=order)
       y_predicted = model.estimate(x_test)
       plt.plot(x_test, y_predicted, "-", label="fit $O(x^{})$".format(order))
       y_p = model.estimate(x)
       mad = np.mean(np.abs(y_p - y))
   plt.legend(fancybox=True)


.. plot::

   from matplotlib import pyplot as plt
   import numpy as np


   a = np.asarray([1, 2, 4, 9, 3, 2, 1, 5])
   b = np.asarray([3, 5, 3, 8, 2, 2, 3, 4])
   y = a / (a + b)
   x = np.arange(len(y))
   variance = a * b / ((a + b + 1) * (a + b)**2)
   w = 1. / variance

   from kvit.ortho import orthogonal_fit


   x_test = np.linspace(x.min(), x.max())

   plt.errorbar(x, y, yerr=variance, fmt="-o", label="data")
   for order in range(2, 8):
       model = orthogonal_fit(x, y, w, order=order)
       y_predicted = model.estimate(x_test)
       plt.plot(x_test, y_predicted, "-", label="fit $O(x^{})$".format(order))
       y_p = model.estimate(x)
       mad = np.mean(np.abs(y_p - y))

   plt.legend(fancybox=True)

.. plot::

   from matplotlib import pyplot as plt
   import numpy as np


   N = 10
   x = np.arange(N)
   y = np.sin((x - x.max()) * np.pi /2)
   w = np.ones_like(x).astype(float)
   y[5] = 10
   w[5] = 1e-1

   from kvit.ortho import orthogonal_fit


   x_test = np.linspace(x.min(), x.max())

   plt.errorbar(x, y, yerr=np.sqrt(1/w), fmt="-o", label="data")
   for order in range(2, 8):
       model = orthogonal_fit(x, y, w, order=order)
       y_predicted = model.estimate(x_test)
       plt.plot(x_test, y_predicted, "-", label="fit $O(x^{})$".format(order))
       y_p = model.estimate(x)
       mad = np.mean(np.abs(y_p - y))

   plt.legend(fancybox=True)


Ansatz
======


A system of orthogonal polynomials :math:`p_k(x)` in the domain
:math:`x_1 \leq x \leq x_n` can be synthesized from simple
functions :math:`x^\nu`

.. math::

  p_k(x) = \sum_{\nu=0}^k b_{k\nu} x^\nu

With :math:`N` data points :math:`(x_i, y_i)`, each being associated with a
variance in :math:`y` of :math:`\sigma_i^2`, we can characterise the
orthogonality-requirement of the polynomials by

.. math::
   :label: orthogonality

   \sum_{i=1}^N \frac{1}{\sigma_i^2} p_j(x_i) p_k(x_i) = \begin{cases}
        1 & j=k \\
        0 & j \neq k
        \end{cases}


To make an estimate, we just have to superimpose the basis
functions, with coefficient for each basis:

.. math::

   \hat{y}_i = \sum^{m-1}_{j=0} a_j p_j(x_i)

So fitting orthogonal polynomials to data points :math:`(x_i, y_i)` means, that
first we need to obtain orthonormal polynomials :math:`p_j(x)` and then
determine coefficients :math:`a_j` for said polynomials.



--------------------------
Orthogonal Basis Functions
--------------------------

First, let's assume we have found suitable basis functions :math:`p_j(x_i)`,
and think of a way to estimate the :math:`a_j`.  Of course we know, that our
regression is not capable of estimating the exact true target value
:math:`y_i`, but we can infer the expectancy :math:`\mathcal{E}[x_i] =
\hat{y_i}`. And we know that this :math:`\hat{y}_i` is exactly the value that
minimizes the standard deviation:

.. math::

   S = \sum_i w_i \left( \hat{y}_i - \sum^{m=1}_{j=0} a_j p_j(x_i) \right)^2

-------------------
Recurrence Relation
-------------------

For any kind of orthogonal polynomial, there is a [recurrence
relation](https://www.wikiwand.com/en/Orthogonal_polynomials#/Recurrence_relation),
that connects orthogonal polynomials of different order:

.. math::
   :label: recurrence

   \gamma_j p_j(x) = (x - \alpha_j) p_{j-1}(x) - \beta_j p_{j-2}(x)

This equation directly shows that :math:`p_{i+1}(x)` is of higher polynomial
degree than :meth:`p_i(x)`.


Solving for α, β, γ
-------------------

Then, we can use :eq:`orthogonality` to solve :eq:`recurrence` for the
parameters :math:`\alpha_j`, :math:`\beta_j`, and :math:`\gamma_j`.

.. math::

   \alpha_j &= \sum_{i=1}^n w_i x_i p_{j-1}^2(x_i)

   \beta_j &= \sum_{i=1}^n w_ix_ip_{j-1}(x_i)p_{j-2}(x_i)

   \gamma_j &= \sqrt{\sum_{i=1}^N w_i \left[(x - \alpha_j) p_{j-1}(x) - \beta_j p_{j-2}(x)\right]^2}

This means once we have two consecutive polynomials :math:`p_0(x)` and
:math:`p_1(x)`, we can construct all higher-order polynomials by determining
:math:`\alpha_j`, :math:`\beta_j`, and :math:`\gamma_j`.

Zero and First Order Polynomial
-------------------------------

The zero-order polynomial is :math:`p_0(x)`, and it is
independent of :math:`x`, (:math:`\sim \mathcal{O}(1)`), thus
:math:`p_0(x) = b_{00}`. The requirement of orthonormality :eq:`orthogonality`
gives us

.. math::

   1 &= \sum_{i=1}^N w_i p_0^2(x) = \sum_{i=1}^N w_i b_{00}^2

   b_{00} &= \frac{1}{\sqrt{\sum\limits_{i=1}^n w_i}}

For the second order  we assume :math:`p_1(x) = b_{10} + b_{11}x` and 
obtain two equations from the orthonormality requirement

.. math::

   1 &= \sum_{i=1}^N w_i (b_{10} + b_{11}x_i)^2 = 1

   0 &= \sum_{i=1}^N w_i (b_{10} + b_{11}x_i) b_{00}

   b_{11} &= \frac{1}{\sqrt{\sum\limits_{i=1}^n w_i\left(x_i - \langle x\rangle\right)^2}}

   b_{10} &= - \langle x\rangle b_{11}



Using :math:`\alpha_j, \beta_j, \gamma_j, p_0(x), p_1(x)` we can obtain all
:math:`p_j(x)`. It is worth noticing, that the target data
points :math:`y_i` did not have any role in determining the
:math:`p_k(x)`, it's solely dependent on the :math:`x_i` and
weights :math:`w_i`.

Implementation Notes
--------------------

:cite:`2012:blobel` presents a FORTRAN 77 implementationo of such a fit,
without much comment, is very verbose and does not abstract away anything.
All values and coefficients are concretely represented in FORTRAN arrays.
This implementation makes heavy use of :class:`numpy.poly1d`, but essentially
leverages the same properties.


