====================
Linear Models & Fits
====================

Linear models give estimates for a target :math:`y` as a linear-combination of
coefficients coefficients :math:`\hat{a}\in \mathbf{R}^{P}` and functions that
depend on the features :math:`x_i`, which we call :math:`f_j(x)`.

.. math::

   \hat{y}_i = \sum_{j=1}^P \hat{a}_j f_j(x_i)


Simple Multivariate Linear Models
=================================

The methods in this section use the case of simple functions :math:`f_j`.


.. math::

   \hat{y}_i(x_{ij}) = a_0 + \sum_{j=1}^P \hat{a}_j x_{ij}


In the context of least-squares methods, the estimate is obtained by minimizing
the sum of squares :math:`S` with respect to the coefficients :math:`\hat{a}`.

.. math::

  S = \sum_{i=1}^N (y_i - \hat{y}_i)^2 = \sum_{i=1}^N \left(y_i -  \sum_{j=1}^P \hat{a}_j f_j(x_i)\right)^2


Model
-----


.. autoclass:: kvit.linear.LinearRegressionModel
   :members:

Fit
---

For fitting a linear model, Kvit provides

.. autofunction:: kvit.linear.linear_regression_model

   asdf

Bayesian Linear Regression
==========================

Following :cite:`2006:bishop`.  This fit assumes a prior distribution for
the coefficients of

.. math::
   \wp\left(\vec{w}\mid \alpha\right) = \mathcal{N}\left(\vec{w}\mid 0, \alpha^{-1} \delta_{ij}\right)

:math:`\beta` is a known noise precision parameter.


.. plot::

   >>> from kvit.linear import bayesian_linear_regression_model
   >>> from matplotlib import pyplot
   >>> import numpy as np
   >>> N = 15
   >>> beta = 0.1
   >>> x = np.linspace(0, 1, N)
   >>> y = np.poly1d([1.4, 2])(x) +  np.random.normal(scale=beta, size=N)
   >>> _ = pyplot.plot(x.reshape((-1, 1)), y, "o")
   >>> model = bayesian_linear_regression_model(x, y, alpha=1e-5, beta=beta)
   >>> x = np.linspace(0, 1, 300)
   >>> y_estimated = model.estimate(x)
   >>> _ = pyplot.plot(x, y_estimated)
   >>> _ = pyplot.title("Bayes")


API
---

Fitting

.. autofunction:: kvit.linear.bayesian_linear_regression_model
