.. kvit documentation master file, created by
   sphinx-quickstart on Wed Feb 22 20:38:06 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

kvit |version|
==============

A Python data-science library aimed at small to intermediate sized datasets.


Version: |version|
Release: |release|

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   linear
   ortho
   kalman
   playground
   em




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


References
==========

.. bibliography:: refs.bib
