#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from pkg_resources import get_distribution

extensions = ['sphinx.ext.mathjax',
              'sphinx.ext.autodoc',
              'sphinxcontrib.bibtex',
              'matplotlib.sphinxext.plot_directive',
             ]


templates_path = ['_templates']

source_suffix = '.rst'

master_doc = 'index'

project = 'kvit'
copyright = '2017, Holger Peters'
author = 'Holger Peters'

release = get_distribution('kvit').version
version = '.'.join(release.split('.')[:2])
language = None

exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

pygments_style = 'sphinx'

html_theme = 'alabaster'
html_static_path = ['_static']

epub_title = project
epub_author = author
epub_publisher = author
epub_copyright = copyright
epub_exclude_files = ['search.html']
