========================
Expectation Maximization
========================


Gaussian Mixture
================

A Gaussian mixture, given mixture coefficients :math:`0 \leq \pi_k \leq 1` of
multidimensional normal distributions can be expressed by

.. math::

   \wp(\vec{x}) = \sum_{k=1}^K \pi_k \mathcal{N} \left(\vec{x} \mid \vec{\mu}, \Sigma_k\right)

Ansatz with Latent Variables
----------------------------

Introducing a latent variable :math:`\vec{z}\in\{0,
1\}^K`, with the constraint that only element of
:math:`\vec{z}` is nonzero:

.. math::

   \sum_{k=1}^K z_k &= 1

   \wp(z_k=1) &= \pi_k


The marginal distribution of :math:`\vec{z}` can then be expressed as a product
of terms :math:`\pi_k^{z_k}`, because :math:`\pi_k^0=1` and :math:`\pi_k^1 =
\pi_k`.

.. math::

   \wp(\vec{z}) = \prod_{k=1}^K \pi_k^{z_k}

The multivariate (joint) distribution of :math:`\vec{x},\vec{z}` can be
expressed by basic laws of probability as

.. math::

   \wp(\vec{x}, \vec{z}) = \wp(\vec{x} \mid \vec{z})\wp(\vec{z})


And the conditional probability for :math:`\vec{x}`, given our condition
:math:`\vec{z}` is

.. math::

   \wp(\vec{x}, \vec{z}) = \prod_{k=1}^K \left[\mathcal{N}(\vec{x} \mid \vec{\mu}_k, \Sigma_k)\right]^{\pi_k}

From this we can obtain the marginal distribution for :math:`\vec{x}` by
marginalizing over the joint distribution, and obtain the relationship we
started with.

.. math::

   \wp(\vec{x}) = \sum_{\vec{z}} \wp(\vec{x}\mid\vec{z}) \wp(\vec{z}) = \sum_{k=1}^K \pi_k \mathcal{N}(\vec{x}\mid \vec{\mu}_k , \Sigma_k)


The Responsibility
------------------

The probability for the k-th component of the :math:`\vec{z}` vector of the
latent variable, given a value :math:`\vec{x}`, i.e. :math:`\wp(z_k \mid
\vec{x})` is called the *responsibility* and can be obtained by Bayes' theorem

.. math::

   \wp(z_k = 1 \mid \vec{x}) &= \frac{\wp(z_k=1) \wp(\vec{x}\mid z_k = 1)}{\wp(\vec{x})}

   &= \frac{\pi_k \mathcal{N}(\vec{x} \mid \vec{\mu}_k, \Sigma_k)}{\sum_{k=1}^K \pi_k \mathcal{N}(\vec{x}\mid \vec{\mu}_k , \Sigma_k)}

Log-Likelihoods
---------------

If we have several vectors :math:`\{\vec{x}_0, \dots\}`,
we can build a matrix :math:`X`, where each row is
:math:`\vec{x}_n^T`.

The likelihood-function is the probability of the data, given the model. I.e.
:math:`\wp(\vec{x}\mid \pi_k, \vec{\mu}_k, \Sigma_k)`.

.. math::

   \wp(\vec{x}\mid \pi_k, \vec{\mu}_k, \Sigma_k) = \sum_{k=1}^K \pi_k \mathcal{N}(\vec{x} \mid \vec{\mu}_k, \Sigma_k)

This leads to likelihoods for a whole data-set

.. math::

   \wp(X \mid \pi_k, \vec{\mu}_k, \Sigma_k) &= \prod_{n=1}^N \left[ \sum_{k=1}^K \pi_k \mathcal{N}(\vec{x}_n \mid \vec{\mu}_k, \Sigma_k)\right]

   \mathcal{L} = \log \wp(X \mid \pi_k, \vec{\mu}_k, \Sigma_k) &= \sum_{n=1}^N \log\left[ \sum\limits_{k=1}^K \pi_k \mathcal{N}(\vec{x}_n \mid \vec{\mu}_k, \Sigma_k)\right]


Maximizing the Likelihood
-------------------------

We want to enforce the constraint on :math:`\pi_k`, so we add a Lagrange
modifier to the likelihood function we want to maximize:

.. math::

   \mathcal{L}' &= \mathcal{L} + \lambda \left(\sum\limits_{k=1}^K \pi_k - 1\right)

   0 &= \frac{\partial \mathcal{L}' }{\partial \vec{\mu}_k}

   0 &= -\sum_{n=1}^N \gamma(z_{nk}) \Sigma_k (\vec{x}_n - \vec{\mu}_k)

   \vec{\mu}_k &= \frac{1}{N_k} \sum_{n=1}^N \gamma(z_{nk}) \vec{x}_n

   N_k &= \sum_{n=1}^N \gamma(z_{nk})

   \Sigma_k &= \frac{1}{N_k} \sum_{n=1}^N \gamma(z_{nk}) (\vec{x}_n - \vec{\mu}_k) (\vec{x}_n - \vec{\mu})^T

   0 &= \frac{\partial \mathcal{L}' }{\partial \pi_k}

   \pi_k &= \frac{N_k}{N}
