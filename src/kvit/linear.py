from numpy.linalg import pinv, inv
import numpy as np


class LinearRegressionModel:
    def __init__(self, *, coefficients, variance):
        self.coefficients = coefficients
        self.variance = variance

    def _design_matrix(self, features):
        return np.c_[np.ones_like(features), features]

    def estimate(self, features: np.ndarray):
        """Return the estimate for the expectancy value of the target, given
        ``features``"""
        estimate = np.dot(self._design_matrix(features), self.coefficients)
        return estimate.reshape(-1)

    def covariance_estimate(self, features):
        """Return the estimate for the covariance matrix of the target"""
        design_matrix = self._design_matrix(features)
        return design_matrix.dot(self.variance).dot(design_matrix.T)


def linear_regression_model(features, target) -> LinearRegressionModel:
    """Returns a linear regression model"""
    n_rows, _ = features.shape
    design_matrix = np.c_[np.ones_like(features), features]
    target = np.c_[target]
    assert n_rows == len(target)
    coefficients = pinv(design_matrix).dot(target)
    variance = inv(design_matrix.T.dot(design_matrix))
    return LinearRegressionModel(coefficients=coefficients, variance=variance)


def linear_regression_model_with_uncertainties(features, target, target_variances) -> LinearRegressionModel:
    assert target.shape == target_variances.shape
    n_rows, _ = features.shape
    design_matrix = np.c_[np.ones_like(features), features]
    target = np.c_[target]
    assert n_rows == len(target)
    w = np.diag(1 / target_variances)
    variance = inv(design_matrix.T.dot(w).dot(design_matrix))
    coefficients = variance.dot(design_matrix.T).dot(w).dot(target)
    return LinearRegressionModel(coefficients=coefficients, variance=variance)


def bayesian_linear_regression_model(features, target, *, beta, alpha=1e-15) -> LinearRegressionModel:
    r"""This fit assumes :math:`\beta` (``beta``) is a known noise precision
    parameter, :math:`\alpha` (``alpha``) is the value of a normal prior on the
    coefficients.
    """
    design_matrix = np.c_[np.ones_like(features), features]
    s_n = inv(alpha * np.eye(2) + beta * design_matrix.T.dot(design_matrix))
    means = beta * s_n.dot(design_matrix.T).dot(target)
    return LinearRegressionModel(coefficients=means.T, variance=s_n)


__all__ = ["linear_regression_model", "LinearRegressionModel"]
