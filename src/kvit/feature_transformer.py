import pandas as pd
import numpy as np
from matplotlib import pyplot
from kvit.ortho import orthogonal_fit


def _posterior_mean(x):
    signal = 0.5 + (x == 1).sum()
    background = 0.5 + (x == 0).sum()
    return signal / (signal + background)


def _posterior_var(x):
    signal = 0.5 + (x == 1).sum()
    background = 0.5 + (x == 0).sum()
    return signal * background / ((signal + background + 1) * (signal + background)**2)


def beta_smoother(*, frame, feature, target, n_bins, equal_statistics=True):
    df = pd.read_csv(
        'https://statweb.stanford.edu/~tibs/ElemStatLearn/datasets/SAheart.data', index_col=0)
    feature_name_new = feature + "_grouped"

    if equal_statistics:
        noise = 0.001 * np.random.randn(len(df[feature]))
        noise = 0
        df[feature_name_new], bins = pd.qcut(
            df[feature] + noise, q=n_bins,
            retbins=True)
        df[feature_name_new] = df[feature_name_new].cat.codes
    else:
        df[feature_name_new] = pd.cut(df[feature], bins=n_bins).cat.codes

    profile = df.groupby([feature_name_new])['chd'].agg(
        mean=_posterior_mean,
        var=_posterior_var
    )
    x = profile.index.values
    y = profile['mean'].values
    weight = 1. / profile['var'].values
    s = np.sqrt(profile['var'].values)
    pyplot.errorbar(x, y, yerr=s, fmt='o')
    m = orthogonal_fit(x, y, weight)
    x_smoothed = np.linspace(x.min(), x.max())
    smoothed = m.estimate(x_smoothed)
    pyplot.plot(x_smoothed, smoothed)
    pyplot.title("Profile of {}".format(feature))
